# NER-BiLSTM-CRF-PyTorch
PyTorch implementation of BiLSTM-CRF and Bi-LSTM-CNN-CRF models for POS tagging.

## Requirements
- Python 3
- PyTorch 1.x

## Papers
- Bidirectional LSTM-CRF Models for Sequence Tagging (Huang et. al., 2015)
  - the first paper apply BiLSTM-CRF to NER
- Neural Architectures for Named Entity Recognition (Lample et. al., 2016)
  - introducing character-level features: pre-trained word embedding（skip-n-gram）with character-based word embeddings trained by RNN
- End-to-end Sequence Labeling via Bi-directional LSTM-CNNs-CRF (Ma et al., 2016)
  - character-level information trained by CNNs
- A Deep Neural Network Model for the Task of Named Entity Recognition （Le et al., 2018)
  - capitalization features
## Dataset
- Universal Dependencies in French

### Evaluation
- conlleval: Perl script used to calculate FB1 (**phrase level**)

## Model
- Embeddings
  - 300d pre-trained word embedding with FastText in French (Lample et. al., 2016)
  - 25d charactor embedding trained by CNNs (Ma et al., 2016)
- BiLSTM-CRF (Ma et al., 2016)

## Exemple of use

```
python3 train.py --train ../UDs/UD_fr/train.qtok/sequoia.conll --dev ../UDs/UD_fr/dev.qtok/sequoia.conll --test ../UDs/UD_fr/test.qtok/sequoia.conll --epoch 2
```

