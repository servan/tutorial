# Classification task: Sentiment analysis

## Classification approaches
### Using LSTM:
```
python3 ./sentiment_analysis_classification_cnn.py --model lstm
```

### Using CNN:
```
python3 ./sentiment_analysis_classification_cnn.py --model cnn
```

### Using Transformers:
```
python3 transformers_classification.py --model qwant/fralbert-base --train ../data/train_small.csv --test ../data/test_small.csv --valid ../data/valid_small.csv --epochs 10
```

## Results

Results with weighted scores.

Using 10 epochs for each models using 1 only run with 1 GPU A100 on JeanZay.



|  Model             | Accuracy  | Precision | Recall   | F1       | Time (s) |
|--------------------|-----------|-----------|----------|----------|----------|
|  LSTM              |   0.56    |   0.89    |   0.56   |  0.66    |    **13**|
|  CNN               |   0.65    |   **0.90**|   0.65   |  0.71    |    38    |
|  CamemBERT-large   |   0,66    |   0,80    |  0,66    |  0,72    |    2943  |
|  CamemBERT-base    |   **0,76**|   0,84    |  **0,76**|  **0,80**|    935   |
|  CamemBERT-ccnet   |   0,58    |   0,78    |  0,58    |  0,66    |    933   |
|  CamemBERT-ccnet-4g|   0,63    |   0,79    |  0,63    |  0,70    |    935   |
|  CamemBERT-oscar-4g|   0,71    |   0,82    |  0,71    |  0,76    |    930   |
|  CamemBERT-wiki-4g |   0,63    |   0,79    |  0,63    |  0,71    |    920   |
|  FrALBERT-wiki-4g  |   0.48    |   0,73    |  0.48    |  0.58    |    985   |
|  FrALBERTv2 (beta) |   0,51    |   0,76    |  0.51    |  0.61    |    983   |
