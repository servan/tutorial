#!/bin/bash



export MAX_LENGTH=256
export BERT_MODEL=$1
export data_dir=../UDs/UD_fr/
if [ ! -e "$data_dir/labels.txt" ]
then
cat $data_dir"/train.txt" $data_dir"/dev.txt" $data_dir"/test.txt" | cut -d " " -f 2 | grep -v "^$"| sort | uniq > $data_dir"/labels.txt"
fi
export BATCH_SIZE=128
export NUM_EPOCHS=100
export SAVE_STEPS=1500
export SEED=1

export OUTPUT_DIR=FineTune.$BATCH_SIZE.$NUM_EPOCHS/$BERT_MODEL

srun python3 run_slu.py --data_dir $data_dir \
--model_type albert \
--labels $data_dir"/labels.txt" \
--model_name_or_path $BERT_MODEL \
--tokenizer_name $BERT_MODEL \
--output_dir $OUTPUT_DIR \
--max_seq_length  $MAX_LENGTH \
--num_train_epochs $NUM_EPOCHS \
--per_gpu_train_batch_size $BATCH_SIZE \
--save_steps $SAVE_STEPS \
--num_train_epochs $NUM_EPOCHS \
--overwrite_output_dir \
--keep_accents \
--do_train \
--do_eval \
--seed $SEED \
--do_predict 


