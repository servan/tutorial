# TutoriAL
Files and git used during "TutoriAL : Apprentissage Profond pour le TAL français pour les débutants"

## Slides

All the slides presented are in PDF format in the "slides" directory.

## Examples

### POS tagging 
See in the related directory

### Sentiment analysis
See in the classification directory
